﻿namespace EveScanner.Evepraisal
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;

    using EveScanner.Core;
    using EveScanner.Interfaces;
    using EveScanner.IoC;


    /// <summary>
    /// This is a modified version of the Evepraisal connector which uses
    /// the JSON APIs I implemented.
    /// </summary>
    public class SkypraisalSvc : IAppraisalService
    {
        /// <summary>
        /// Holds the URI for the appraisal service.
        /// </summary>
        protected string uri = string.Empty;

        /// <summary>
        /// Holds Scan Data
        /// </summary>
        protected string scanData = string.Empty;

        /// <summary>
        /// Holds scan url
        /// </summary>
        protected string scanUrl = string.Empty;

        /// <summary>
        /// Holds appraisal response.
        /// </summary>
        protected string appraisalResponse = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="SkypraisalSvc"/> class.
        /// </summary>
        public SkypraisalSvc()
        {
            this.uri = "https://skyblade.de/";
        }

        /// <summary>
        /// Gets a ScanResult for a particular set of data you want to appraise.
        /// </summary>
        /// <param name="data">Items to appraise</param>
        /// <returns>Parsed ScanResult</returns>
        public IScanResult GetAppraisalFromScan(IEnumerable<ILineAppraisal> data)
        {
            this.scanUrl = string.Empty;
            this.scanData = string.Join(Environment.NewLine, data.Select(x => x.ToString()).ToArray());
            this.appraisalResponse = GetAppraisalFromScanData();
            try
            {
                EvepraisalAppraisalJson json = EvepraisalAppraisalJson.Resolve(this.appraisalResponse);
                return new ScanResult(
                    Guid.Empty,
                    DateTime.Now,
                    this.scanData,
                    (decimal)json.appraisal.Totals.Buy,
                    (decimal)json.appraisal.Totals.Sell,
                    !string.IsNullOrEmpty(this.scanData) ? this.scanData.Split(new string[] { Environment.NewLine, "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries).Length : json.appraisal.Items.Count(),
                    (decimal)json.appraisal.Totals.Volume,
                    this.uri + "a/" + json.appraisal.Id,
                    json.appraisal.Items);
            }
            catch
            {
                string s = "SCAN-" + DateTime.Now.ToString("yyyyMMddhhmmss", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(this.scanData))
                {
                    File.WriteAllText(s + ".req.txt", this.scanData);
                }

                if (!string.IsNullOrEmpty(this.scanUrl))
                {
                    File.WriteAllText(s + ".req.txt", this.scanUrl);
                }

                File.WriteAllText(s + ".rsp.txt", this.appraisalResponse);

                Logger.Error("Scan Parsing Failed! Logged scan data to " + s + ".*.txt", true);

                throw;
            }
        }

        /// <summary>
        /// Gets a ScanResult for a previously submitted appraisal.
        /// </summary>
        /// <param name="url">Previous Appraisal URL</param>
        /// <returns>Parsed ScanResult</returns>
        public IScanResult GetAppraisalFromUrl(string url)
        {
            return null;
        }

        /// <summary>
        /// Indicates if this service can handle a URL presented to it.
        /// </summary>
        /// <param name="url">URL to previously submitted appraisal</param>
        /// <returns>True if the service can handle the URL, false otherwise.</returns>
        public bool CanRetrieveFromUrl(string url)
        {
            return false;
        }

        /// <summary>
        /// Posts a set of data to Evepraisal for a new appraisal.
        /// </summary>
        /// <returns>Page HTML</returns>
        protected string GetAppraisalFromScanData()
        {
            Logger.Debug("Scan Data: {0}", this.scanData);
            string data = Uri.EscapeDataString(this.scanData);

            try
            {
                Uri localUri = new Uri(this.uri + "appraisal.json");
                string requestString = "market=jita&raw_textarea=" + data;
                Logger.Debug("Request String: {0}", requestString);
                byte[] encodedBytes = Encoding.UTF8.GetBytes(requestString);

                using (IWebClient cli = Injector.Create<IWebClient>())
                {
                    string responseFromServer = cli.PostUriToString(localUri, encodedBytes);
                    Logger.Debug("Response Html: {0}", responseFromServer);

                    return responseFromServer;
                }
            }
            catch (Exception ex)
            {
                Logger.Debug("Error", ex.ToString());
                throw;
            }
        }

    }
}
