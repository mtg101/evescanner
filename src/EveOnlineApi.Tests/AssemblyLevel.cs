﻿using EveOnlineApi.Interfaces;
using EveOnlineApi.Interfaces.Xml;
using EveScanner.IoC;
using NUnit.Framework;

namespace EveOnlineApi.Tests
{
    [SetUpFixture]
    public class AssemblyLevel
    {
        [OneTimeSetUp]
        public static void AssemblyInit()
        {
            // Configure XML Handling
            Injector.Register<IAllianceXmlDataProvider>(typeof(FileBackedEveOnlineXmlApi));
            Injector.Register<ICharacterXmlDataProvider>(typeof(FileBackedEveOnlineXmlApi));
            Injector.Register<ICorporationXmlDataProvider>(typeof(FileBackedEveOnlineXmlApi));
            Injector.Register<IContactListXmlDataProvider>(typeof(FileBackedEveOnlineXmlApi));

            // Configure other API Entity Injections
            Injector.Register<IAllianceDataProvider>(typeof(XmlBackedEveOnlineApi));
            Injector.Register<ICharacterDataProvider>(typeof(XmlBackedEveOnlineApi));
            Injector.Register<ICorporationDataProvider>(typeof(XmlBackedEveOnlineApi));
            Injector.Register<IStandingsDataProvider>(typeof(XmlBackedEveOnlineApi));
        }
    }
}
